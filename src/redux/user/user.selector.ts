import { createSelector } from '@reduxjs/toolkit';
import { AppState } from '../store';

export const selectUser = (state: AppState) => state.user;

export const selectIsAuth = createSelector(
  selectUser,
  (user) => Boolean(user.email),
);
