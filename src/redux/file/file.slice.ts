import { createAction } from '@reduxjs/toolkit';

export const fileUploadRequest = createAction<string>('FILE_UPLOAD_REQUEST'); 
export const fileUploadError = createAction<string>('FILE_UPLOAD_ERROR');
export const fileUploadSuccess = createAction<string>('FILE_UPLOAD_SUCCESS');