 
import React from 'react';
import { MusicNote, InsertDriveFile, Image } from '@mui/icons-material';
import { SvgIconProps } from '@mui/material';
import { isAudioFile, isImageFile } from '@src/utils/helpers';

type Props = SvgIconProps & {
  fileName: string,
  // animating?: boolean,
};

export default function FileIcon({ fileName, ...restProps } : Props) {
  switch (true) {
    case isAudioFile({ name: fileName }):
      return <MusicNote {...restProps} />;

    case isImageFile({ name: fileName }):
      return <Image {...restProps} />;

    default:
      return <InsertDriveFile {...restProps} />;
  }
}
