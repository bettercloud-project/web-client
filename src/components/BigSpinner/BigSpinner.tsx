import React from 'react';

import useStyles from './BigSpinner.styles';
import { Modal } from '@mui/material';
import LoopIcon from '@mui/icons-material/Loop';

type BigSpinnerProps = {
  isLoading: boolean;
}

export default function BigSpinner({ isLoading }: BigSpinnerProps) {
  const classes = useStyles();
  return (
    <Modal
      open={isLoading}
      onClose={() => undefined}
      className={classes.modal}
     >
      <LoopIcon className={classes.icon} />
    </Modal>
  );
}