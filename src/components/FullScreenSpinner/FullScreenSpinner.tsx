
import React from 'react';
import { useSelector } from 'react-redux';

import { selectLoading } from '@src/redux/spinner/spinner.selector';
import BigSpinner from '@src/components/BigSpinner';


export default function FullScreenSpinnerRedux() {
  const isLoading = useSelector(selectLoading);
  return (
    <BigSpinner isLoading={isLoading} />
  );
}