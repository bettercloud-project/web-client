import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(({ palette }: Theme) => ({
  root: {},
  divider: {
    margin: '15px 5px',
  },
  playerContainer: {
    minHeight: 50,
    minWidth: 300,
    padding: 5,
    backgroundColor: palette.mode === 'dark' ? palette.background.default : '#548ea9',
  },
  audio: {
    width: '100%',
    '&:focus': {
      outline: 'none',
    },
    // backgroundColor: palette.background.paper,
  },
  displayNone: {
    display: 'none',
  },
}), { name: 'AudioPlayer' });

export default useStyles;
