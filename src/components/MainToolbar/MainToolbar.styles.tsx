import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  toolbarRoot: {
    justifyContent: 'space-between',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  navLink: {
    fontSize: '1.25rem',
    fontWeight: 500,
    textDecoration: 'none',
    color: 'white',
    '&.active': {
      textDecoration: 'underline',
    },
  },
}), { name: 'MainToolbar' });
export default useStyles;
