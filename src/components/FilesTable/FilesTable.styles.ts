import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  root: {},
  cursor: {
    cursor: 'pointer',
  },
  shared: {
    border: '1px solid grey',
    borderRadius: '5px',
  },
}));

export default useStyles;
