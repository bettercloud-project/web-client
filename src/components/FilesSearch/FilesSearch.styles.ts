import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    marginRight: theme.spacing(2),
    marginLeft: 'auto',
  },
  input: {
    padding: '5px 10px',
    flexBasis: '240px',
    transition: 'flex-basis 400ms ease',
    backgroundColor: theme.palette.action.selected,
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    '&.Mui-focused': {
      flexBasis: '80%',
    },
    borderRadius: '5px',
    color: 'white',
  },
}), { name: 'FilesSearch' });

export default useStyles;
