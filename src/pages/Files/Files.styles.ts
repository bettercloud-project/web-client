import { makeStyles } from '@mui/material';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
  },
  progressBar: {
    top: 0,
    left: 0,
  },
}));

export default useStyles;
