import { createTheme } from '@mui/material';
import { useEffect, useMemo, useState } from 'react';
import ThemeService from '../services/Theme.service';

export default function useThemeSwitcher() {
  const [darkTheme, setDarkTheme] = useState(ThemeService.preferredTheme);
  const theme = useMemo(() => createTheme({
    colorSchemes: {
      dark: darkTheme,
    },
  }), [darkTheme]);

  useEffect(() => {
    ThemeService.darkTheme = darkTheme;
  }, [darkTheme]);

  return [theme, darkTheme, setDarkTheme] as const;
}
