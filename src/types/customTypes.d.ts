/* eslint-disable @typescript-eslint/no-explicit-any */
declare type S3File = {
  Key: string,
  LastModified?: Date,
  ETag?: string,
  Size: number,
  StorageClass?: string,
  name: string,
  type: 'file' | 'directory',
};

declare type Timestamps = {
  created_at: string;
  updated_at: string;
}

declare module 'file-size' {
  interface FileSizeParams {
    /**
     * @param {number} fixed Number of positions after the decimal to show, default is 2
     * @param {string} spacer Space between the number and the unit, default is a space
     */
    fixed?: number;
    spacer?: string;
  }
  type StringSpec = 'si' | 'iec' | 'jedec';
  type Unit = 'B' | 'KB' | 'MB' | 'GB' | 'TB' | 'PB' | 'EB' | 'ZB' | 'YB';

  type CalculateOutput = {
    suffix: Unit,
    magnitude: number,
    result: number,
    fixed: string,
    bits: { result: number, fixed: string }
  };
  interface FileSizeOutput {
    /**
     * @description Generates human readable filesize.
     * @param {string} spec si, iec or jedec Specification.
     */
    human: (spec?: StringSpec) => string;

    /**
     * @description Converts bytes to another filesize unit
     * @param {string} unit Unit of size
     * @param {string} spec si, iec or jedec Specification.
     */
    to: (unit: Unit, spec?: StringSpec) => number

    /**
     * @description Calculates suffix, magnitude,
     *  fixed, non-fixed, bits (fixed, non-fixed) from specified bytes against the specified spec.
     * @param {string} spec si, iec or jedec Specification.
     */
    calculate: (spec?: StringSpec) => CalculateOutput;
  }
  export default function (bytes: number, params?: FileSizeParams): FileSizeOutput;
}

declare module 'react-audio-spectrum'

// create a generic type
declare type AsyncReturnType<T extends (...args: any) => any> =

  // if T matches this signature and returns a Promise, extract
  // U (the type of the resolved promise) and use that, or...
  T extends (...args: any) => Promise<infer U> ? U :

  // if T matches this signature and returns anything else,
  // extract the return value U and use that, or...
    T extends (...args: any) => infer U ? U :

    // if everything goes to hell, return an `any`
      any;

declare type UserGroup = {
  id: number;
  name: 'user' | 'admin' | 'guest';
} & Timestamps;

declare type User = {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  groups: UserGroup[] | [];
} & Timestamps;

declare type FolderPermission = {
  id: number;
  path: string;
  type: 'public' | 'private'
} & Timestamps;
