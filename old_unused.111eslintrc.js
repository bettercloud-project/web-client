module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb-typescript',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020,
    sourceType: 'module',
    project: './tsconfig.json',
  },
  plugins: [
    'react',
    '@typescript-eslint',
    'import',
  ],
  rules: {
  },
  overrides: [
    {
      files: ['**/*.tsx'],
      rules: {
        'react/prop-types': 'off',
        'react/jsx-filename-extension': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react/static-property-placement': 'off',
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": ["error"]
      }
    },
    {
      files: ['**/*.ts'],
      rules: {
        '@typescript-eslint/lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }]

      }
    },
    {
      files: ['**/*.tsx', '**/*.ts'],
      rules: {
        'max-len': ["error", { "ignoreComments": true, code: 120 }]
      }
    }
  ],
};
